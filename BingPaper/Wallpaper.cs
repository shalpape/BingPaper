﻿using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace BingPaper
{
    public class Wallpaper
    {
        private const uint SpiSetdeskwallpaper = 0x14;
        private const uint SpifUpdateinifile = 0x01;
        private const uint SpifSendwininichange = 0x02;

        public static void Set(string filePath)
        {
            var key = Registry.CurrentUser.OpenSubKey("Control Panel\\Desktop", true);

            if (key != null)
            {
                key.SetValue("WallpaperStyle", "10"); // Wallpaper Style 1 = Fit, 2 = Stretch, 10 = Fill
                key.SetValue("TileWallpaper", "0"); // Disable tiling
            }

            if (!NativeMethods.SystemParametersInfo(SpiSetdeskwallpaper, 0, filePath, SpifUpdateinifile | SpifSendwininichange)) return;
            // Save error code:
            var ex = new Win32Exception(Marshal.GetLastWin32Error());
            if (!ex.NativeErrorCode.Equals(0))
                throw ex;
        }
    }
}