﻿using System;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;

namespace BingPaper
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private BingWallPaperClient _bingWallPaperClient;

        private int _index;
        private int _lastNPhotos = 1;

        public int LastNPhotos
        {
            get => _lastNPhotos;

            set => _lastNPhotos = value;
        }

        public int Index
        {
            get => _index;

            set => _index = value;
        }

        public string Market { get; set; } = "de-DE";

        public string TargetPath { get; set; } =
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), Assembly.GetExecutingAssembly().GetName().Name + @"\");

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (e.Args.Length > 0 && (e.Args[0].Equals("/?") || e.Args[0].Equals("/help") || e.Args[0].Equals("--help") || e.Args[0].Equals("-h")))
                DisplayHelp();
            else
                for (var i = 0; i < e.Args.Length; i++)
                    if ((e.Args[i].ToLower().Equals("-l") || e.Args[i].ToLower().Equals("--lastnpictures")) && e.Args.Length > i + 1 &&
                        int.TryParse(e.Args[i + 1], out _lastNPhotos))
                    {
                        Console.WriteLine(@"Value for parameter --lastnpictures is: {0}", LastNPhotos);
                        i++;
                    }
                    else if ((e.Args[i].ToLower().Equals("-i") || e.Args[i].ToLower().Equals("--index")) && e.Args.Length > i + 1 &&
                             int.TryParse(e.Args[i + 1], out _index))
                    {
                        Console.WriteLine(@"Value for parameter --index is: {0}", Index);
                        i++;
                    }
                    else if ((e.Args[i].ToLower().Equals("-m") || e.Args[i].ToLower().Equals("--market")) && e.Args.Length > i + 1)
                    {
                        Market = e.Args[i + 1];
                        // TODO: check regex
                        if (!Regex.IsMatch(Market, "^[a-z]{2}-[A-Z]{2}$"))
                        {
                            Console.WriteLine(@"Bad parameter or value specified: {0}", e.Args[i]);
                            DisplayHelp();
                        }
                        Console.WriteLine(@"Value for parameter --market is: {0}", Market);
                        i++;
                    }
                    else if ((e.Args[i].ToLower().Equals("-t") || e.Args[i].ToLower().Equals("--targetpath")) && e.Args.Length > i + 1)
                    {
                        TargetPath = e.Args[i + 1];
                        if (!TargetPath.EndsWith(@"\"))
                            TargetPath = TargetPath + @"\";
                        Console.WriteLine(@"Value for parameter --targetpath is: {0}", TargetPath);
                        i++;
                    }
                    else
                    {
                        Console.WriteLine(@"Bad parameter or value specified: {0}", e.Args[i]);
                        DisplayHelp();
                    }

            if (!Directory.Exists(TargetPath))
                try
                {
                    Directory.CreateDirectory(TargetPath);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(@"An error occurred when validating parameter --targetPath.");
                    Console.WriteLine(@"The error message is:");
                    Console.WriteLine(exception.Message);
                    DisplayHelp();
                }

            bool executeApp;

            // Make sure this program isn't already running
            //string appGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value.ToString();
            var mutex = new Mutex(false, "Global\\bingpaper"); //+ appGuid);
            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl,
                AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);
            mutex.SetAccessControl(securitySettings);

            try
            {
                executeApp = mutex.WaitOne(0);
            }
            catch (AbandonedMutexException)
            {
                executeApp = true;
            }

            if (executeApp)
            {
                // Check for internet connection before updating background
                if (NetworkInterface.GetIsNetworkAvailable())
                    UpdateBackground();

                // Release the mutex
                mutex.ReleaseMutex();
            }

            // That's it, shut 'er down
            Shutdown();
        }

        private static void DisplayHelp()
        {
            var assembly = Assembly.GetEntryAssembly();
            Console.WriteLine();
            Console.WriteLine(@"{0} v{1}", assembly.GetName().Name, assembly.GetName().Version);
            Console.WriteLine(((AssemblyCopyrightAttribute) assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false).GetValue(0)).Copyright);
            Console.WriteLine();
            Console.WriteLine(@"{0} downloads the Bing picture of the day and sets it as a wallpaper.", assembly.GetName().Name);
            Console.WriteLine();
            Console.WriteLine(@"Usage: {0} [-l n] [-t <path to target folder>] [-m <market>] [-i n]", assembly.GetName().Name);
            Console.WriteLine();
            Console.WriteLine(@"Options:");
            Console.WriteLine(@"  -h  --help            Displays this help text.");
            Console.WriteLine(@"  -i  --index           Sets the index (days in the past) for the picture to download.");
            Console.WriteLine(@"  -l  --lastnpictures   Downloads the last n pictures of the day.");
            Console.WriteLine(@"                        Up to eight pictures are available.");
            Console.WriteLine(@"  -m  --market          Sets the market from where the photo should be downloaded.");
            Console.WriteLine(@"                        Example given: de-DE or en-US");
            Console.WriteLine(@"  -t  --targetpath      Sets the local folder where the pictures should be saved.");
            Console.WriteLine(@"                        A subfolder named BingPaper will be created at the target location.");
            Environment.Exit(0);
        }

        private void UpdateBackground()
        {
            _bingWallPaperClient = new BingWallPaperClient(LastNPhotos, Index, Market, TargetPath);
            _bingWallPaperClient.Download();
            if (!_bingWallPaperClient.PhotoOfTheDayList.Any()) return;

            try
            {
                Wallpaper.Set(_bingWallPaperClient.PhotoOfTheDayList[0].LocalPhotoFilePath);
            }
            catch (Exception exception)
            {
                File.WriteAllText(TargetPath + @"error.dat", exception.Message);
                Console.WriteLine(@"An error occurred while trying to set the wallpaper.");
                Console.WriteLine(@"The error message is:");
                Console.WriteLine(exception.Message);
            }
        }
    }
}