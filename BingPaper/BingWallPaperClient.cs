﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BingPaper
{
    /// <summary>
    ///     A class designed to download the bing photo of the day
    /// </summary>
    public class BingWallPaperClient
    {
        private readonly string _feed;
        private readonly string _localTargetPath;
        private bool _loadcalled;
        private List<BingPhotoOfTheDay> _photoOfTheDayList = new List<BingPhotoOfTheDay>();

        /// <summary>
        ///     Creates a new instance of the Bing photo downloader
        /// </summary>
        /// <param name="lastNPhotos">Specifies the number of pictures to download. 8 is maximum.</param>
        /// <param name="idx">Specifies the index of the picture. You can go back 7 days.</param>
        /// <param name="market">Specifies the market from where to download the picture. Something like en-US or de-DE</param>
        /// <param name="targetPath">Specifies the path where the pictures should be saved. Default is %temp% directory.</param>
        public BingWallPaperClient(int lastNPhotos = 1, int idx = 0, string market = "de-DE", string targetPath = null)
        {
            _localTargetPath = targetPath ?? Environment.ExpandEnvironmentVariables("%temp%") + @"\";
            _loadcalled = false;

            //photo of the day data in xml format
            _feed = "http://www.bing.com/HPImageArchive.aspx?format=xml&idx=" + idx + "&n=" + lastNPhotos + "&mkt=" + market;
        }

        public List<BingPhotoOfTheDay> PhotoOfTheDayList
        {
            get
            {
                if (!_loadcalled) throw new InvalidOperationException("Call the Download() method first");
                return _photoOfTheDayList;
            }

            set => _photoOfTheDayList = value;
        }

        /// <summary>
        ///     Downloads the photo of the day syncronously
        /// </summary>
        public void Download()
        {
            var imageNodes = XDocument.Load(_feed).Elements().Elements().Where(n => n.Name == "image");

            foreach (var imageNode in imageNodes)
            {
                string photoFilePath = null;
                string copyrightFilePath = null;

                var bingPhotoOfTheDay = new BingPhotoOfTheDay();
                var urlBase = (from i in imageNode.Elements() where i.Name == "urlBase" select i.Value).FirstOrDefault();

                if (urlBase != null)
                {
                    bingPhotoOfTheDay.PhotoUrl = "http://www.bing.com" + urlBase + "_1920x1080.jpg";
                    var photoFileName = urlBase.Split('/').LastOrDefault() + "_1920x1080.jpg";
                    if (photoFileName.Contains("th?id=OHR.")) photoFileName = photoFileName.Replace("th?id=OHR.", "");
                    photoFilePath = _localTargetPath + photoFileName;
                    var copyrightFileName = urlBase.Split('/').LastOrDefault() + ".txt";
                    if (copyrightFileName.Contains("th?id=OHR.")) copyrightFileName = copyrightFileName.Replace("th?id=OHR.", "");
                    copyrightFilePath = _localTargetPath + copyrightFileName;
                }

                var copyright = (from i in imageNode.Elements() where i.Name == "copyright" select i.Value).FirstOrDefault();
                var copyrightLink = (from i in imageNode.Elements() where i.Name == "copyrightlink" select i.Value).FirstOrDefault();
                bingPhotoOfTheDay.CopyrightData = copyright + Environment.NewLine + copyrightLink;

                if (copyrightFilePath != null)
                {
                    File.WriteAllText(copyrightFilePath, bingPhotoOfTheDay.CopyrightData);
                    bingPhotoOfTheDay.LocalCopyrightFileInfo = new FileInfo(copyrightFilePath);
                }

                if (photoFilePath != null)
                {
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(bingPhotoOfTheDay.PhotoUrl, photoFilePath);
                    }
                    bingPhotoOfTheDay.LocalPhotoFileInfo = new FileInfo(photoFilePath);
                }

                _photoOfTheDayList.Add(bingPhotoOfTheDay);
            }

            _loadcalled = true;
        }

        /// <summary>
        ///     Asyncronous & awaitable version of the download routine
        /// </summary>
        /// <returns>An awaitable task</returns>
        public Task DownloadAsync()
        {
            return Task.Run(() => Download());
        }
    }

    public class BingPhotoOfTheDay
    {
        public string CopyrightData { get; set; }
        public string PhotoUrl { get; set; }
        public FileInfo LocalPhotoFileInfo { get; set; }
        public FileInfo LocalCopyrightFileInfo { get; set; }
        public string PhotoFileName => LocalPhotoFileInfo.Name;
        public string LocalPhotoFilePath => LocalPhotoFileInfo.FullName;
        public string CopyrightFileName => LocalCopyrightFileInfo.Name;
        public string LocalCopyrightFilePath => LocalCopyrightFileInfo.FullName;
    }
}