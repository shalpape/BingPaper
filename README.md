# BingPaper
## Description
BingPaper downloads the Bing picture of the day and sets it as a wallpaper.
## Usage
BingPaper is a console application. So you can call the application with the following parameters.

    BingPaper [-l n] [-t <path to target folder>] [-m <market>] [-i n]

    Options:
      -h  --help            Displays this help text.
      -i  --index           Sets the index (days in the past) for the picture to download.
      -l  --lastnpictures   Downloads the last n pictures of the day.
                            Up to eight pictures are available.
      -m  --market          Sets the market from where the photo should be downloaded.
                            Example given: de-DE or en-US
      -t  --targetpath      Sets the local folder where the pictures should be saved.
                            A subfolder named BingPaper will be created at the target location.

If you just double click the executable, it will be executed with the following default values.

|Parameter|Value|
|:-|:-|
|--index|0|
|--lastnpictures|1|
|--market|de-DE|
|--targetpath|Folder BingPaper in special folder MyPictures|
